#!/bin/bash

ip_address=$(hostname -I)

hostname=$(hostname)

os_info=$(cat /etc/os-release | grep "^PRETTY_NAME=" | cut -d= -f2 | tr -d '"')

echo "Welcome to $hostname!"
echo "IP Address: $ip_address"
echo "OS: $os_info"

